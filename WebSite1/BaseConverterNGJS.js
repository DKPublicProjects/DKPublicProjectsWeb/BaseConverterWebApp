﻿

var app = angular.module("BaseConverterApp", []);

var convert = function (decimalValue, base) {
	var resultString = "";
	var countHelper;

	while (decimalValue !== 0) {
		countHelper = (decimalValue % base);
		if (countHelper >= 10) {
			resultString = String.fromCharCode((countHelper - 10) + 65) + resultString;
		} else if (countHelper >= 36) {
			resultString = String.fromCharCode((countHelper - 36) + 97) + resultString;
		} else {
			resultString = countHelper + resultString;
		}
		decimalValue = Math.floor(decimalValue / base);
	}
	return resultString;
}


app.controller("BaseCount",
	function($scope) {
		$scope.recountToBase = function(decimalValue, base) {
			if (decimalValue === undefined || base === undefined || decimalValue === null || base === null)
				return "";
			if (base <= 1) {
				return "Sorry, wrong base";
			}
			var resultString = "";
			var countHelper;
			if (decimalValue < 0) {
				return "-" + convert(-decimalValue, base);
			} else {
				return convert(decimalValue, base);
			}
		}
	});

